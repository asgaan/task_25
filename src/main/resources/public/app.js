
// Adding extra contacts
let k=0;
function addContact(){

    var container = document.getElementById("contactContainer");
    container.appendChild(document.createElement("br"));

    container.appendChild(document.createTextNode("Number: "));
    var input = document.createElement("input");
    input.type = "text";
    input.id = "number" + k;
    container.appendChild(input);
    container.appendChild(document.createElement("br"));


    container.appendChild(document.createTextNode("Type of number: "));
    
    var select = document.createElement("select");
    select.id= "numberType" + k;
    
    var option1 = document.createElement("option");
    var option2 = document.createElement("option");
    var option3 = document.createElement("option");
    option1.value = "Private";
    option1.innerHTML="Private";
    option2.value = "Home";
    option2.innerHTML = "Home";
    option3.value = "Work";
    option3.innerHTML="Work";

    select.appendChild(option1);
    select.appendChild(option2);
    select.appendChild(option3);
    container.appendChild(select);
    container.appendChild(document.createElement("br"));

    k++;
}

// Adding extra emails
let i=0;
function addEmail(){

    var container = document.getElementById("emailContainer");
    container.appendChild(document.createElement("br"));

    container.appendChild(document.createTextNode("Email: "));

    var input = document.createElement("input");
    input.type = "text";
    input.id = "mail" + i;
    container.appendChild(input);
    container.appendChild(document.createElement("br"));


    container.appendChild(document.createTextNode("Email type: "));
    
    var select = document.createElement("select");
    select.id= "mailType" + i;
    
    var option1 = document.createElement("option");
    var option2 = document.createElement("option");
    option1.value = "Private";
    option1.innerHTML="Private";
    option2.value = "Work";
    option2.innerHTML="Work";

    select.appendChild(option1);
    select.appendChild(option2);
    container.appendChild(select);
    container.appendChild(document.createElement("br"));

    i++;
}

// Converts relationship type to relationship id
function returnRelationID(relID) {
    let id1;
    if(relID === "Mother") {
        id1 = 1;
    }
    else if(relID === "Father") {
        id1 = 2;
    }
    else if(relID === "Brother") {
        id1 = 3;
    }
    else if(relID === "Sister") {
        id1 = 4;
    }
    else if(relID === "Son") {
        id1 = 5;
    }
    else if(relID === "Daughter") {
        id1 = 6;
    }
    return id1;
}

function submit(){

    let firstName = document.getElementById("firstName").value;
    let lastName = document.getElementById("lastName").value;
    let dateOfBirth = document.getElementById("dateOfBirth").value;
    let stringDate = dateOfBirth.toString();

    let streetName = document.getElementById("streetName").value;
    let streetNumber = document.getElementById("streetNumber").value;
    let postNumber = document.getElementById("postNumber").value;
    let postName = document.getElementById("postName").value;

    let contactNumber= document.getElementById("contactNumber").value;
    let numberType = document.getElementById("numberType").value;

    let emailAddress = document.getElementById("emailAddress").value;
    let emailType = document.getElementById("emailType").value;

    let relativeFirstName;
    let relativeLastName;
    let exists = document.getElementById("sel");

    if(!exists.value == "") {
        let name = document.getElementById("msg");
        let nameChild = name.childNodes[1].innerHTML;
        let nameSplit = nameChild.split(" ");


        relativeFirstName = nameSplit[0];
        relativeLastName = nameSplit[1];
    }
    let relationshipType = document.getElementById("relationshipType").value;
    let reverseRelationshipType = document.getElementById("reverseRelationshipType").value;




    let obj = {
        first_name:firstName,
        last_name:lastName,
        date_of_birth:stringDate,
        numbers:[{
            number:contactNumber,
            number_type:numberType
        }],
        emails:[{
            email:emailAddress,
            type:emailType
        }],
        address:{
            street_name:streetName,
            street_number:streetNumber,
            post:
                {
                    post_name:postName,
                    post_number:postNumber
                }},
        relatives: []
    };

    // Adding relatives if not empty
    if(!relativeFirstName == "") {
        let first_name = relativeFirstName;
        let last_name = relativeLastName;
        let relationship_name = relationshipType;
        let relationship_id = returnRelationID(relationship_name);
        let reverse_relationship_id = returnRelationID(reverseRelationshipType);
        let relativeObject = {first_name, last_name, relationship_name, relationship_id, reverse_relationship_id};
        obj.relatives.push(relativeObject);
    }

    //Adding extra emails to the object.
    for(j=0; j < i; j++) {
        let email = document.getElementById("mail"+j).value;
        let email_type = document.getElementById("mailType"+j).value;
        let mailObject = {email, email_type};
        obj.emails.push(mailObject);
    }

    //Adding extra contacts to the object.
    for(j=0; j < k; j++) {
        let number = document.getElementById("number"+j).value;
        let number_type = document.getElementById("numberType"+j).value;
        let contactObject = {number, number_type};
        obj.numbers.push(contactObject);
    }

    //SEND myJSON
    // Creating a XHR object 
    let xhr = new XMLHttpRequest(); 
    let url = "http://localhost:8080/persons";

    // open a connection 
    xhr.open("POST", url, true); 

    // Set the request header i.e. which type of content you are sending 
    xhr.setRequestHeader("Content-Type", "application/json"); 

    // Create a state change callback 
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4 && xhr.status === 200) {

            // Print received data from server
          //  let result;
          //  result.innerHTML = this.responseText;
        }
    };
    // Converting JSON data to string
    let myJSON = JSON.stringify(obj);
    // Sending data with the request 
    xhr.send(myJSON);
    console.log(myJSON);
    
}

window.onload = populate();
function populate() {
    // CREATE AN XMLHttpRequest OBJECT, WITH GET METHOD.
    let xhr = new XMLHttpRequest(),
        method = 'GET',
        overrideMimeType = 'application/json',
        url = 'http://localhost:8080/persons';        // ADD THE URL OF THE FILE.
    xhr.onreadystatechange = function () {
        if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
            // PARSE JSON DATA.
            let data = JSON.parse(xhr.responseText);
            console.log(data);
            let el = document.getElementById('sel');
            for(let i = 0; i < data.length; i++) {
                el.innerHTML = el.innerHTML + '<option value ="' + data[i].id + '">' + data[i].first_name + " " + data[i].last_name + '</option>';
            }
        }
    };
    xhr.open(method, url, true);
    xhr.send();
}
function show(el) {
    let msg = document.getElementById('msg');
    msg.innerHTML = 'Selected Relative: <b>' + el.options[el.selectedIndex].text + '</b> </br>' +
        'ID: <b>' + el.value + '</b>';
}