package no.noroff.accelerated.task25grouptask3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import static no.noroff.accelerated.task25grouptask3.DAODB.getPersons;
import static no.noroff.accelerated.task25grouptask3.DAODB.openConn;

@SpringBootApplication
public class Task25GroupTask3Application {

	public static void main(String[] args) {
		DAODB db = new DAODB();
		SpringApplication.run(Task25GroupTask3Application.class, args);
	}

}
