package no.noroff.accelerated.task25grouptask3.models;

public class PostNumber {
    public String post_name;
    public int post_number;

    public PostNumber(String post_name, int post_number) {
        this.post_name = post_name;
        this.post_number = post_number;
    }

    public String getPost_name() {
        return post_name;
    }

    public void setPost_name(String post_name) {
        this.post_name = post_name;
    }

    public int getPost_number() {
        return post_number;
    }

    public void setPost_number(int post_number) {
        this.post_number = post_number;
    }

    @Override
    public String toString() {
        return "PostNumber{" +
                "post_name='" + post_name + '\'' +
                ", post_number=" + post_number +
                '}';
    }
}
