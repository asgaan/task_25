package no.noroff.accelerated.task25grouptask3.models;

public class ContactNumber {
    public String number;
    public String number_type;

    public ContactNumber(String number, String number_type) {
        this.number = number;
        this.number_type = number_type;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumber_type() {
        return number_type;
    }

    public void setNumber_type(String number_type) {
        this.number_type = number_type;
    }

    @Override
    public String toString() {
        return "ContactNumber{" +
                "number='" + number + '\'' +
                ", number_type='" + number_type + '\'' +
                '}';
    }
}
