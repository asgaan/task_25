package no.noroff.accelerated.task25grouptask3.models;
import java.util.ArrayList;

public class Person {
    public int id;
    public String first_name;
    public String last_name;
    public String date_of_birth;
    public int address_id;
    public ArrayList<ContactNumber> numbers;
    public ArrayList<Email> emails;
    public Address address;
    public ArrayList<Family> relatives;

    public Person(int id, String first_name, String last_name, String date_of_birth, int address_id, ArrayList<ContactNumber> numbers, ArrayList<Email> emails, Address address, ArrayList<Family> relatives) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.date_of_birth = date_of_birth;
        this.address_id = address_id;
        this.numbers = numbers;
        this.emails = emails;
        this.address = address;
        this.relatives = relatives;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public ArrayList<ContactNumber> getNumbers() {
        return numbers;
    }

    public void setNumbers(ArrayList<ContactNumber> numbers) {
        this.numbers = numbers;
    }

    public ArrayList<Email> getEmails() {
        return emails;
    }

    public void setEmails(ArrayList<Email> emails) {
        this.emails = emails;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public ArrayList<Family> getRelatives() {
        return relatives;
    }

    public void setRelatives(ArrayList<Family> relatives) {
        this.relatives = relatives;
    }

    public int getAddress_id() {
        return address_id;
    }

    public void setAddress_id(int address_id) {
        this.address_id = address_id;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", date_of_birth='" + date_of_birth + '\'' +
                ", address_id=" + address_id +
                ", numbers=" + numbers +
                ", emails=" + emails +
                ", address=" + address +
                ", relatives=" + relatives +
                '}';
    }
}
