package no.noroff.accelerated.task25grouptask3.models;

public class Address {
    public String street_name;
    public String street_number;
    public PostNumber post;

    public Address(String street_name, String street_number, PostNumber post) {
        this.street_name = street_name;
        this.street_number = street_number;
        this.post = post;
    }

    public String getStreet_name() {
        return street_name;
    }

    public void setStreet_name(String street_name) {
        this.street_name = street_name;
    }

    public String getStreet_number() {
        return street_number;
    }

    public void setStreet_number(String street_number) {
        this.street_number = street_number;
    }

    public PostNumber getPost() {
        return post;
    }

    public void setPost(PostNumber post) {
        this.post = post;
    }

    @Override
    public String toString() {
        return "Address{" +
                "street_name='" + street_name + '\'' +
                ", street_number='" + street_number + '\'' +
                ", post=" + post.toString() +
                '}';
    }
}
