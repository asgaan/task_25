package no.noroff.accelerated.task25grouptask3.models;

public class Family {
    public String first_name;
    public String last_name;
    public String relationship_name;
    public int relationship_id;
    public int reverse_relationship_id;

    public Family(String first_name, String last_name, String relationship_name, int relationship_id, int reverse_relationship_id) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.relationship_name = relationship_name;
        this.relationship_id = relationship_id;
        this.reverse_relationship_id = reverse_relationship_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getRelationship_name() {
        return relationship_name;
    }

    public void setRelationship_name(String relationship_name) {
        this.relationship_name = relationship_name;
    }

    public int getRelationship_id() {
        return relationship_id;
    }

    public void setRelationship_id(int relationship_id) {
        this.relationship_id = relationship_id;
    }

    public int getReverse_relationship_id() {
        return reverse_relationship_id;
    }

    public void setReverse_relationship_id(int reverse_relationship_id) {
        this.reverse_relationship_id = reverse_relationship_id;
    }
}

