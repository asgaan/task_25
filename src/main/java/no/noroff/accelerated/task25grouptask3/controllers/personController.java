package no.noroff.accelerated.task25grouptask3.controllers;

import jdk.jshell.PersistentSnippet;
import no.noroff.accelerated.task25grouptask3.DAODB;
import no.noroff.accelerated.task25grouptask3.models.Person;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
public class personController {

    DAODB db = new DAODB();
    private AtomicInteger nextId = new AtomicInteger();

    public personController() {
        DAODB.openConn();
        DAODB.getPersons();
    }

    @RequestMapping("/persons")
    public ArrayList<Person> personFind() {
        return DAODB.persons;
    }

    @GetMapping("/person/{id}")
    public Person personGet(@PathVariable int id) {
        System.out.println("Trying to find person: " + id);
        Person returnPerson = null;
        for(Person p : DAODB.persons) {
            if(p.getId() == id) {
                returnPerson = p;
            }
        }
        if(returnPerson == null) {
            System.out.println("--- PERSON NOT FOUND ---");
        }
        return returnPerson;
    }

    @PostMapping("/persons")
    public Person registerPerson(@RequestBody Person person) {
        int id = DAODB.insert(person);
        person.setId(id);
        DAODB.persons.add(person);

        return person;
    }

    @DeleteMapping("/person/{id}")
    public ResponseEntity<Integer> deletePerson(@PathVariable int id) {
        System.out.println("Trying to find person to delete: " + id);
        boolean removed = DAODB.persons.removeIf(p -> p.getId() == id);
        DAODB.deletePerson(id);

        if(!removed) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(id, HttpStatus.OK);
    }
}
