package no.noroff.accelerated.task25grouptask3;

import no.noroff.accelerated.task25grouptask3.models.*;
import org.springframework.web.bind.annotation.PostMapping;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class DAODB {

    private static String username = "vlvvklhatpddkx";
    private static String password = "a816c626dec870027c95f6a39a5c9aa359cce473c1b4bf757a61da84e5e8e2c1";
    private static String dbUrl = "jdbc:postgresql://ec2-46-137-177-160.eu-west-1.compute.amazonaws.com:5432/d8vab26i9oj1p6?sslmode=require&user=" + username + "&password=" + password;
    private static Connection conn = null;
    public static ArrayList<Person> persons = new ArrayList<Person>();

    public DAODB() {

    }

    public static void openConn() {
        try {
            conn = DriverManager.getConnection(dbUrl);
            System.out.println("Connection to DB has been established");

        } catch (Exception e) {
            System.out.println("Something went wrong.");
            System.out.println(e.getMessage());
        }
    }

    public static ArrayList<Person> getPersons() {
        try {
            String sql = "select id, first_name, last_name, date_of_birth, address_id from Person";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                persons.add(
                        new Person(rs.getInt("id"),
                                rs.getString("first_name"),
                                rs.getString("last_name"),
                                rs.getString("date_of_birth"),
                                rs.getInt("address_id"),
                                getContactNumbers(rs.getInt("id")),
                                getEmails(rs.getInt("id")),
                                getAddress(rs.getInt("address_id")),
                                getFamilyMembers(rs.getInt("id")))
                );
            }
        } catch (SQLException e) {
            System.out.println("Something went wrong.");
            System.out.println(e.getMessage());
        }
        return persons;
    }

    public static ArrayList<Email> getEmails(int id) {
        ArrayList<Email> email = new ArrayList<Email>();
        try {
            String sql = "select email, email_type from Email where person_id=" + id;
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                email.add(new Email(rs.getString("email"), rs.getString("email_type")));
            }

        } catch (SQLException e) {
            System.out.println("Something went wrong.");
            System.out.println(e.getMessage());
        }
        return email;
    }

    public static ArrayList<Family> getFamilyMembers(int id) {
        ArrayList<Family> family = new ArrayList<Family>();
        try {
            String sql = "select p2.first_name as RelativeFirstName, p2.last_name as RelativeLastName, r.relationship_name, r.id as id, f.reverse_relationship_id as rid, p1.first_name " +
                    "from Family as f, Relationship as r, Person as p1, Person as p2 " +
                    "where p1.id =" + id + " AND ((p1.id = f.Person_id_1 AND p2.id = f.Person_id_2 AND f.Relationship_id = r.id) OR (p1.id = f.Person_id_2 AND p2.id = f.Person_id_1 AND f.reverse_relationship_id = r.id))";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                family.add(new Family(rs.getString("RelativeFirstName"),
                        rs.getString("RelativeLastName"),
                        rs.getString("relationship_name"),
                        rs.getInt("id"),
                        rs.getInt("rid")));
            }

        } catch (SQLException e) {
            System.out.println("Something went wrong.");
            System.out.println(e.getMessage());
        }
        return family;
    }

    public static ArrayList<ContactNumber> getContactNumbers(int id) {
        ArrayList<ContactNumber> numbers = new ArrayList<ContactNumber>();
        try {
            String sql = "select number, number_type from ContactNumber where person_id=" + id;
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                numbers.add(new ContactNumber(rs.getString("number"), rs.getString("number_type")));
            }

        } catch (SQLException e) {
            System.out.println("Something went wrong.");
            System.out.println(e.getMessage());
        }
        return numbers;
    }

    public static Address getAddress(int id) {
        Address address = null;
        try {
            String sql = "select street_name, street_number, post_number from Address where id=" + id;
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                address = new Address(rs.getString("street_name"), rs.getString("street_number"), getPostNumber(rs.getInt("post_number")));
            }

        } catch (SQLException e) {
            System.out.println("Something went wrong.");
            System.out.println(e.getMessage());
        }
        return address;
    }

    public static PostNumber getPostNumber(int pnr) {
        PostNumber postnr = null;

        try {
            String sql = "select post_number, post_name from postnumber where post_number=" + pnr;
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                postnr = new PostNumber(rs.getString("post_name"), rs.getInt("post_number"));
            }
        } catch (SQLException e) {
            System.out.println("Something went wrong.");
            System.out.println(e.getMessage());
        }
        return postnr;
    }


    public static int insert(Person person) {
        int person_id = person.getId();

        String fname = person.getFirst_name();
        String lname = person.getLast_name();
        String dob = person.getDate_of_birth();
        Address a = person.getAddress();
        PostNumber pnr = person.getAddress().getPost();
        int p = person.getAddress().getPost().getPost_number();

        PreparedStatement ps = null;
        boolean inListPostnumber = false;

        try {
            for (Person object : persons) {
                if (object.getAddress().getPost().getPost_number() == p) {
                    inListPostnumber = true;
                    break;
                }
            }

            if(inListPostnumber) {
                System.out.println("POSTNUMBER ALREADY IN DATABASE");
            } else {
                insertPostNumber(pnr);
            }

            int aid = insertAddress(a, p);

            String sql = "Insert into Person values(default, ?, ?, ?, ?) RETURNING id";
            ps = conn.prepareStatement(sql);
            ps.setString(1, fname);
            ps.setString(2, lname);
            ps.setString(3, dob);
            ps.setInt(4, aid);
            ResultSet rs = ps.executeQuery();
            rs.next();

            int id = rs.getInt("id");

            for (ContactNumber c : person.getNumbers()) {
                insertNumbers(c, id);
            }

            for (Email e : person.getEmails()) {
                insertEmail(e, id);
            }

            String relationshipName = null;
            for (Family relative : person.getRelatives()) {
                relationshipName = relative.getRelationship_name();
            }

            for (Person object : persons) {
                for (Family f : object.getRelatives()) {
                    if (f.getRelationship_name().equals(relationshipName)) {
                        insertRelationship(person.getRelatives(), id);
                    }
                }
            }
            conn.close();
            return id;
        } catch (SQLException e) {
            System.out.println("Something went wrong.");
            System.out.println(e.getMessage());
        }
        return -1;
    }

    public static void insertPostNumber(PostNumber pnr) {
        int nr = pnr.getPost_number();
        String n = pnr.getPost_name();

        try {
            String sql = "Insert into postnumber values(?, ?)";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, nr);
            ps.setString(2, n);
            ps.executeUpdate();

        } catch (SQLException e) {
            System.out.println("Something went wrong with inserting post number.");
            System.out.println(e.getMessage());
        }
    }

    public static int insertAddress(Address a, int pnr) {
        String streetname = a.getStreet_name();
        String streetnumber = a.getStreet_number();

        try {
            String sql = "Insert into address values(default, ?, ?, ?) RETURNING id";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, streetname);
            ps.setString(2, streetnumber);
            ps.setInt(3, pnr);
            ResultSet rs = ps.executeQuery();
            rs.next();

            return rs.getInt("id");

        } catch (SQLException e) {
            System.out.println("Something went wrong with inserting address.");
            System.out.println(e.getMessage());
        }

        return -1;
    }

    public static void insertNumbers(ContactNumber c, int id) {
        String number = c.getNumber();
        String type = c.getNumber_type();

        try {
            String sql = "Insert into contactnumber values(default, ?, ?, ?)";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ps.setString(2, number);
            ps.setString(3, type);
            ps.executeUpdate();

        } catch (SQLException e) {
            System.out.println("Something went wrong with inserting numbers.");
            System.out.println(e.getMessage());
        }
    }

    public static void insertEmail(Email email, int id) {
        String mail = email.getEmail();
        String type = email.getType();

        try {
            String sql = "Insert into email values(default, ?, ?, ?)";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ps.setString(2, mail);
            ps.setString(3, type);
            ps.executeUpdate();

        } catch (SQLException e) {
            System.out.println("Something went wrong with inserting emails.");
            System.out.println(e.getMessage());
        }
    }

    public static void insertRelationship(ArrayList<Family> relatives, int person_id) {
        int relationship_id = 0;
        int relative_id = 0;
        int reverse_relationship_id = 0;

        for (Family relative : relatives) {
            relationship_id = relative.getRelationship_id();
            reverse_relationship_id = relative.getReverse_relationship_id();

            for (Person object : persons) {
                if (object.getFirst_name().equals(relative.getFirst_name()) && object.getLast_name().equals(relative.getLast_name())) {
                    relative_id = object.getId();

                }
            }
        }

        try {
            String sql = "Insert into family values(?, ?, ?, ?)";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, person_id);
            ps.setInt(2, relative_id);
            ps.setInt(3, relationship_id);
            ps.setInt(4, reverse_relationship_id);
            ps.executeUpdate();

        } catch (SQLException e) {
            System.out.println("Something went wrong with inserting Family relations.");
            System.out.println(e.getMessage());
        }
    }

    public static void deletePerson(int id) {
        try {
            String sql = "delete from person where id = " + id; //used cascading delete in PostGreSQL so when id is deleted, all data referencing the id is also deleted(Contactnumber, Email, etc)
            PreparedStatement ps = conn.prepareStatement(sql);
            int deleted = ps.executeUpdate();

            if(deleted == 0) {
                System.out.println("PERSON NOT FOUND IN DATABASE");
            }else {
                System.out.println("PERSON DELETED FROM DATABASE");
            }

        }catch (SQLException e) {
            System.out.println("Something went wrong with deleting person.");
            System.out.println(e.getMessage());
        }
    }
}
